
# No previous file for Guilin
culture = epednar
religion = eotomolaque
capital = ""

hre = no

base_tax = 2
base_production = 2
base_manpower = 1

trade_goods = unknown

native_size = 25
native_ferocity = 9
native_hostileness = 10
1550.1.1 = {
	owner = H65
	controller = H65
	add_core = H65
	culture = dustman
	religion = corinite
	is_city = yes
	tribal_owner = ---
}