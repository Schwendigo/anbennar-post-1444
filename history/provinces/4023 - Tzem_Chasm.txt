



culture = cave_goblin
religion = goblinic_shamanism
trade_goods = unknown
hre = no
base_tax = 1 
base_production = 2
base_manpower = 1
native_size = 63
native_ferocity = 10
native_hostileness = 10

1550.1.1 = {
	owner = Z55
	controller = Z55
	add_core = Z55
	culture = darkscale_kobold
	religion = kobold_dragon_cult
}