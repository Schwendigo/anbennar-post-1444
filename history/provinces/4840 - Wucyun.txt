owner = Y09
controller = Y09
add_core = Y09
add_core = Z58
culture = forest_yan
religion = righteous_path

hre = no

base_tax = 2
base_production = 3
base_manpower = 2

trade_goods = iron

capital = ""

is_city = yes

1550.1.1 = {
	owner = R62
	controller = R62
	add_core = R62
	religion = godlost
}

add_permanent_province_modifier = {
	name = harimari_minority_integrated_small
	duration = -1
}
