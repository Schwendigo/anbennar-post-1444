



culture = straggler_goblin
religion = goblinic_shamanism
trade_goods = unknown
hre = no
base_tax = 1 
base_production = 1
base_manpower = 1
native_size = 78
native_ferocity = 10
native_hostileness = 10
add_permanent_province_modifier = {
	name = dwarovar_rail
	duration = -1
}

1550.1.1 = {
	owner = H87
	controller = H87
	add_core = H87
	culture = straggler_goblin
	religion = goblinic_shamanism
	is_city = yes
}