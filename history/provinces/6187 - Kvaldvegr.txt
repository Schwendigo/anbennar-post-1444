
culture = forest_troll
religion = mountain_watchers
capital = "Kvaldvegr"
trade_goods = unknown
hre = no
base_tax = 1 
base_production = 1
base_manpower = 1
native_size = 25
native_ferocity = 3
native_hostileness = 4

1550.1.1 = {
	owner = Z50
	controller = Z50
	add_core = Z50
	is_city = yes
	culture = grombar_half_orc
	religion = corinite
}