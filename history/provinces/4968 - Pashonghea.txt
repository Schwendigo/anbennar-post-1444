# No previous file for Anbennar4968
owner = Y71
controller = Y71
add_core = Y71
culture = bokai
religion = righteous_path

hre = no

base_tax = 2
base_production = 3
base_manpower = 1

trade_goods = incense

capital = ""

is_city = yes

1550.1.1 = {
	owner = Y72
	controller = Y72
	add_core = Y72
}

add_permanent_province_modifier = {
	name = harimari_minority_coexisting_small
	duration = -1
}