
# No previous file for Abancay
culture = green_orc
religion = great_dookan
capital = ""
tribal_owner = B29
hre = no

base_tax = 1
base_production = 1
base_manpower = 1

trade_goods = salt

native_size = 31
native_ferocity = 8
native_hostileness = 6

1550.1.1 = {
	owner = B33
	controller = B33
	add_core = B33
	is_city = yes
	religion = corinite
	culture = marcher
	tribal_owner = ---
}