owner = Y61
controller = Y61
add_core = Y61
culture = risbeko
religion = mystic_accord

hre = no

base_tax = 2
base_production = 3
base_manpower = 2

trade_goods = tropical_wood

capital = ""

is_city = yes

1550.1.1 = {
	owner = Y58
	controller = Y58
	add_core = Y58
}
