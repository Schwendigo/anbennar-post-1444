
# No previous file for Pehuenmapu
culture = green_orc
religion = great_dookan
capital = ""
tribal_owner = B23
hre = no

base_tax = 3
base_production = 2
base_manpower = 2

trade_goods = unknown
center_of_trade = 2

native_size = 56
native_ferocity = 4
native_hostileness = 9

1550.1.1 = {
	owner = B35
	controller = B35
	add_core = B35
	is_city = yes
	religion = corinite
	culture = ancardian
	tribal_owner = ---
}

