
# No previous file for Huancavelica
culture = green_orc
religion = great_dookan
capital = ""
tribal_owner = B29
hre = no

base_tax = 2
base_production = 2
base_manpower = 1

trade_goods = grain

native_size = 34
native_ferocity = 6
native_hostileness = 8

1550.1.1 = {
	owner = B33
	controller = B33
	add_core = B33
	is_city = yes
	religion = corinite
	culture = marcher
	tribal_owner = ---
}