
# No previous file for Banten
owner = R07
controller = R07
add_core = R07
culture = shandibad
religion = high_philosophy

hre = no

base_tax = 5
base_production = 4
base_manpower = 5

trade_goods = cotton

capital = ""

is_city = yes

1550.1.1 = {
	owner = R62
	controller = R62
	add_core = R62
	religion = godlost
}
add_permanent_province_modifier = {
	name = dwarven_minority_coexisting_small
	duration = -1
}