
# No previous file for Maranhao
culture = green_orc
religion = great_dookan
capital = ""
trade_goods = grain
tribal_owner = B27
hre = no

base_tax = 2
base_production = 2
base_manpower = 2

native_size = 44
native_ferocity = 5
native_hostileness = 7

1550.1.1 = {
	owner = B39
	controller = B39
	add_core = B39
	is_city = yes
	religion = corinite
	culture = heartman
	tribal_owner = ---
	tribal_owner = ---
}