
# No previous file for Cahuilla

owner = B24
controller = B24
add_core = B24
tribal_owner = B24
culture = green_orc
religion = great_dookan
capital = ""
hre = no

base_tax = 1
base_production = 1
base_manpower = 1

trade_goods = fur

native_size = 25
native_ferocity = 8
native_hostileness = 8

1550.1.1 = {
	owner = Z52
	controller = Z52
	add_core = Z52
	religion = corinite
}

add_permanent_province_modifier = {
	name = goblin_minority_oppressed_small
	duration = -1
}
