
# No previous file for Guayaquil

culture = green_orc
religion = great_dookan
capital = ""
tribal_owner = B29
hre = no

base_tax = 2
base_production = 2
base_manpower = 1

trade_goods = unknown

native_size = 15
native_ferocity = 7
native_hostileness = 8

1550.1.1 = {
	owner = B33
	controller = B33
	add_core = B33
	is_city = yes
	religion = corinite
	culture = marcher
	tribal_owner = ---
}

add_permanent_province_modifier = {
	name = human_minority_oppressed_small
	duration = -1
}