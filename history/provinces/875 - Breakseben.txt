
# No previous file for Paiute

culture = green_orc
religion = great_dookan
capital = ""
tribal_owner = B42
hre = no

base_tax = 1
base_production = 1
base_manpower = 1

trade_goods = unknown

native_size = 35
native_ferocity = 8
native_hostileness = 10

1550.1.1 = {
	owner = Z35
	controller = Z35
	add_core = Z35
	is_city = yes
	religion = corinite
	culture = rosanda
	tribal_owner = ---
}