
# No previous file for Palau
culture = kwineh
religion = weeping_mother
capital = ""
tribal_owner = G05

hre = no

base_tax = 2
base_production = 2
base_manpower = 2

trade_goods = unknown

native_size = 17
native_ferocity = 4
native_hostileness = 3

1550.1.1 = {
	owner = Z43
	controller = Z43
	add_core = Z43
	is_city = yes
	culture = Z43
	religion = Z43
	tribal_owner = ---
}