
# No previous file for Banten
owner = R10
controller = R10
add_core = R10
culture = royal_harimari
religion = high_philosophy

hre = no

base_tax = 5
base_production = 5
base_manpower = 5

trade_goods = cotton

capital = ""

is_city = yes

1550.1.1 = {
	owner = R62
	controller = R62
	add_core = R62
	religion = godlost
}


add_permanent_province_modifier = {
	name = human_minority_integrated_large
	duration = -1
}