owner = Y17
controller = Y17
add_core = Y17
culture = northern_yan
religion = righteous_path

hre = no

base_tax = 4
base_production = 4
base_manpower = 3

trade_goods = grain

capital = ""

is_city = yes

1550.1.1 = {
	owner = Y19
	controller = Y19
	add_core = Y19
}
