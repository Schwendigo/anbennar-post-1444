
# No previous file for Uruguay
culture = green_orc
religion = great_dookan
capital = ""
tribal_owner = B46
hre = no

base_tax = 1
base_production = 2
base_manpower = 1

trade_goods = unknown

native_size = 15
native_ferocity = 8
native_hostileness = 8

1550.1.1 = {
	owner = B35
	controller = B35
	add_core = B35
	is_city = yes
	religion = corinite
	culture = ancardian
	tribal_owner = ---
}