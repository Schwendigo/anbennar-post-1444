
# No previous file for Darfur
culture = teira
religion = dotimism
capital = ""
trade_goods = unknown
hre = no
base_tax = 1 
base_production = 1
base_manpower = 1

native_size = 12
native_ferocity = 4
native_hostileness = 4
1550.1.1 = {
	owner = Z08
	controller = Z08
	add_core = Z08
	is_city = yes
	culture = Z08
	religion = Z08
}