
# No previous file for Malindi
culture = teira
religion = dotimism
capital = "Ambercoast"

hre = no

base_tax = 1
base_production = 2
base_manpower = 1

trade_goods = unknown

native_size = 25
native_ferocity = 4
native_hostileness = 4
1550.1.1 = {
	owner = A79
	controller = A79
	add_core = A79
	is_city = yes
	culture = A79
	religion = A79
}