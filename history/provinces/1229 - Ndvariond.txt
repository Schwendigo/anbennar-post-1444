
# No previous file for Berber
culture = teira
religion = dotimism
capital = ""
trade_goods = unknown
hre = no
base_tax = 1 
base_production = 1
base_manpower = 1

native_size = 12
native_ferocity = 4
native_hostileness = 4

add_permanent_province_modifier = {
	name = icebreaker_toll
	duration = -1
}
1550.1.1 = {
	owner = A79
	controller = A79
	add_core = A79
	is_city = yes
	culture = A79
	religion = A79
}