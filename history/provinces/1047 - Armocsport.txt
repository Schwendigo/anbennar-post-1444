
# No previous file for Jugjur
culture = boek
religion = dotimism
capital = ""

hre = no

base_tax = 2
base_production = 2
base_manpower = 1

trade_goods = unknown

native_size = 25
native_ferocity = 4
native_hostileness = 6

1550.1.1 = {
	owner = H36
	controller = H36
	add_core = H36
	is_city = yes
	culture = H36
	religion = H36
	tribal_owner = ---
}