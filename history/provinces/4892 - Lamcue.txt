owner = Y09
controller = Y09
add_core = Y09
add_core = Z59
culture = coastal_yan
religion = righteous_path

hre = no

base_tax = 2
base_production = 2
base_manpower = 2

trade_goods = sugar

capital = ""

is_city = yes

1550.1.1 = {
	owner = R62
	controller = R62
	add_core = R62
	religion = godlost
}
