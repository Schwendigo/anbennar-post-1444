
# No previous file for Chorotega
culture = green_orc
religion = great_dookan
capital = ""
tribal_owner = B30
hre = no

base_tax = 2
base_production = 2
base_manpower = 2

trade_goods = grain

native_size = 55
native_ferocity = 6
native_hostileness = 7

1550.1.1 = {
	owner = B53
	controller = B53
	add_core = B53
	is_city = yes
	religion = corinite
	culture = nurcestiran
	tribal_owner = ---
}
