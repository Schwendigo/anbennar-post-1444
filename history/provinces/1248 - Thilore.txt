
# No previous file for Maldives
culture = ice_sleepers
religion = etchings_of_the_deep
capital = ""

hre = no

base_tax = 1
base_production = 1
base_manpower = 1

trade_goods = unknown

native_size = 11
native_ferocity = 4
native_hostileness = 8



1550.1.1 = {
	owner = H37
	controller = H37
	add_core = H37
	is_city = yes
	culture = H37
	religion = corinite
}