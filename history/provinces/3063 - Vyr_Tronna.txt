
owner = I42
controller = I42
add_core = I42
culture = green_orc
religion = great_dookan
hre = no
is_city = yes

1550.1.1 = {
	owner = I36
	controller = I36
	add_core = I36
}
trade_goods = tropical_wood
base_tax = 1
base_manpower = 1
base_production = 1
native_size = 0
native_ferocity = 10
native_hostileness = 10


add_permanent_province_modifier = {
	name = verdant_link
	duration = -1
}