
# No previous file for Banten
owner = R30
controller = R30
add_core = R30
culture = ghankedhen
religion = high_philosophy

hre = no

base_tax = 1
base_production = 1
base_manpower = 1

trade_goods = livestock

capital = ""

is_city = yes

1550.1.1 = {
	owner = R74
	controller = R74
	add_core = R74
}
