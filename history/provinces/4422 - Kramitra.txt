
# No previous file for Banten
owner = R40
controller = R40
add_core = R40
culture = dhukharuved
religion = high_philosophy

hre = no

base_tax = 5
base_production = 4
base_manpower = 3

trade_goods = grain

capital = ""

is_city = yes

1550.1.1 = {
	owner = F51
	controller = F51
	add_core = F51
	religion = the_jadd
}


add_permanent_province_modifier = {
	name = harimari_minority_coexisting_small
	duration = -1
}
