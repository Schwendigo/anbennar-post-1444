



culture = black_orc
religion = old_dookan
trade_goods = unknown
hre = no
base_tax = 6
base_production = 4
base_manpower = 8
native_size = 0
native_ferocity = 10
native_hostileness = 10
center_of_trade = 1
add_permanent_province_modifier = {
	name = ruined_hold
	duration = -1
}

1550.1.1 = {
	owner = I10
	controller = I10
	add_core = I10
	culture = lead_dwarf
	religion = ancestor_worship
}