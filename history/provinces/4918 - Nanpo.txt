owner = Y16
controller = Y16
add_core = Y16
culture = northern_yan
religion = righteous_path

hre = no

base_tax = 1
base_production = 1
base_manpower = 1

trade_goods = wool

capital = ""

is_city = yes

1550.1.1 = {
	owner = Y91
	controller = Y91
	add_core = Y91
}
