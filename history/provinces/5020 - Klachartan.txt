
culture = fathide_ogre
religion = feast_of_the_gods
is_city = yes
owner = J43
controller = J43
add_core = J43


hre = no

base_tax = 1
base_production = 3
base_manpower = 2
native_size = 4
native_ferocity = 3
native_hostileness = 5

1550.1.1 = {
	owner = I90
	controller = I90
	add_core = I90
}

trade_goods = iron

capital = ""