
# No previous file for Las Khorey
culture = ice_sleepers
religion = etchings_of_the_deep
capital = ""

hre = no

base_tax = 1
base_production = 1
base_manpower = 1

trade_goods = unknown

native_size = 11
native_ferocity = 4
native_hostileness = 8

add_permanent_province_modifier = {
	name = icebreaker_toll
	duration = -1
}
1550.1.1 = {
	owner = Z08
	controller = Z08
	add_core = Z08
	is_city = yes
	culture = Z08
	religion = Z08
}