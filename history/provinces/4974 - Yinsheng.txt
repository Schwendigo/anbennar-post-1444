owner = Y02
controller = Y02
add_core = Y02
culture = hill_yan
religion = lefthand_path

hre = no

base_tax = 2
base_production = 3
base_manpower = 2

trade_goods = iron

capital = ""

is_city = yes

1550.1.1 = {
	owner = R62
	controller = R62
	add_core = R62
	religion = godlost
}
