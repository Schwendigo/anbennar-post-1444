owner = Y30
controller = Y30
add_core = Y30
culture = western_yan
religion = righteous_path

hre = no

base_tax = 4
base_production = 3
base_manpower = 2

trade_goods = grain

capital = ""

is_city = yes

1550.1.1 = {
	owner = R62
	controller = R62
	add_core = R62
	religion = godlost
}
