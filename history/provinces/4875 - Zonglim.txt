owner = Y25
controller = Y25
add_core = Y25
culture = coastal_yan
religion = righteous_path

hre = no

base_tax = 1
base_production = 2
base_manpower = 1

trade_goods = naval_supplies

capital = ""

is_city = yes

1550.1.1 = {
	owner = Y26
	controller = Y26
	add_core = Y26
}
