
# No previous file for San Joaquin
culture = forest_troll
religion = mountain_watchers
capital = "Nednvidr"
trade_goods = unknown
hre = no
base_tax = 1 
base_production = 1
base_manpower = 1
native_size = 50
native_ferocity = 6
native_hostileness = 6

1550.1.1 = {
	owner = Z50
	controller = Z50
	add_core = Z50
	is_city = yes
	culture = grombar_half_orc
	religion = corinite
}