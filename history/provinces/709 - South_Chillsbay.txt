
owner = Z23
controller = Z23
add_core = Z23
culture = white_reachman
religion = regent_court

hre = no

base_tax = 3
base_production = 3
base_manpower = 3

trade_goods = fish

capital = ""

is_city = yes

add_permanent_province_modifier = {
	name = orcish_minority_oppressed_large
	duration = -1
}

1550.1.1 = {
	owner = Z50
	controller = Z50
	add_core = Z50
	culture = grombar_half_orc
	religion = corinite
}
