
# No previous file for Sotuta
culture = green_orc
religion = great_dookan
capital = ""
tribal_owner = B25
hre = no

base_tax = 3
base_production = 2
base_manpower = 2

trade_goods = unknown
center_of_trade = 1

native_size = 68
native_ferocity = 8
native_hostileness = 8

1550.1.1 = {
	owner = Z37
	controller = Z37
	add_core = Z37
	is_city = yes
	religion = corinite
	culture = covenbladic
	tribal_owner = ---
}