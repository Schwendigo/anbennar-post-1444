



culture = black_orc
religion = old_dookan
trade_goods = unknown
hre = no
base_tax = 6
base_production = 6
base_manpower = 7
native_size = 0
native_ferocity = 10
native_hostileness = 10
center_of_trade = 1
add_permanent_province_modifier = {
	name = infested_hold
	duration = -1
}

1550.1.1 = {
	owner = I01
	controller = I01
	add_core = I01
	culture = kronium_dwarf
	religion = ancestor_worship
}