
# No previous file for Banten
owner = R20
controller = R20
add_core = R20
culture = muthadhaya
religion = high_philosophy

hre = no

base_tax = 6
base_production = 7
base_manpower = 5

trade_goods = silk

capital = ""

is_city = yes

1550.1.1 = {
	owner = R62
	controller = R62
	add_core = R62
	religion = godlost
}
fort_15th = yes


add_permanent_province_modifier = {
	name = harimari_minority_coexisting_large
	duration = -1
}
