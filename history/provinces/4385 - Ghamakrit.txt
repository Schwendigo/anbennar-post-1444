
# No previous file for Banten
owner = R41
controller = R41
add_core = R41
culture = rabhidarubsad
religion = high_philosophy

hre = no

base_tax = 6
base_production = 5
base_manpower = 4

trade_goods = cotton

capital = ""

is_city = yes

1550.1.1 = {
	owner = F51
	controller = F51
	add_core = F51
	religion = the_jadd
}
