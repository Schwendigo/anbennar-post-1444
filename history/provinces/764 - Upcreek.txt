
# No previous file for Santo Amaro
culture = green_orc
religion = great_dookan
capital = ""
tribal_owner = B46
hre = no

base_tax = 2
base_production = 1
base_manpower = 1

trade_goods = unknown

native_size = 34
native_ferocity = 8
native_hostileness = 9

1550.1.1 = {
	owner = B40
	controller = B40
	add_core = B40
	is_city = yes
	religion = corinite
	culture = rogieran
	tribal_owner = ---
}