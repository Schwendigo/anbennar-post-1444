owner = Y54
controller = Y54
add_core = Y54
culture = risbeko
religion = mystic_accord

hre = no

base_tax = 2
base_production = 2
base_manpower = 2

trade_goods = tropical_wood

capital = ""

is_city = yes

1550.1.1 = {
	owner = R38
	controller = R38
	add_core = R38
}
