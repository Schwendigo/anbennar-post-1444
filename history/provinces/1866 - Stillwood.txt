
# No previous file for Namur
culture = cheshoshi
religion = death_cult_of_cheshosh
capital = ""

hre = no

base_tax = 1
base_production = 1
base_manpower = 1

trade_goods = unknown

native_size = 20
native_ferocity = 5
native_hostileness = 7

1550.1.1 = {
	owner = G96
	controller = G96
	add_core = G96
	is_city = yes
	culture = G96
	religion = G96
	tribal_owner = ---
}