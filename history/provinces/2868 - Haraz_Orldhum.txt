



culture = black_orc
religion = old_dookan
trade_goods = unknown
hre = no
base_tax = 5
base_production = 5
base_manpower = 8
native_size = 0
native_ferocity = 10
native_hostileness = 10
center_of_trade = 1
add_permanent_province_modifier = {
	name = infested_hold
	duration = -1
}

1550.1.1 = {
	owner = Z55
	controller = Z55
	add_core = Z55
	culture = darkscale_kobold
	religion = kobold_dragon_cult
}