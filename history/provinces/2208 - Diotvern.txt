
# No previous file for Terek
culture = kwineh
religion = weeping_mother
capital = ""

hre = no

base_tax = 1
base_production = 1
base_manpower = 1

trade_goods = unknown

native_size = 17
native_ferocity = 4
native_hostileness = 3

1550.1.1 = {
	owner = Z43
	controller = Z43
	add_core = Z43
	is_city = yes
	culture = Z43
	religion = Z43
	tribal_owner = ---
}