
# No previous file for Uliastai
owner = Z18
controller = Z18
add_core = Z47
add_core = Z18
culture = gray_orc
religion = old_dookan
capital = ""
trade_goods = fur

hre = no

base_tax = 2
base_production = 2
base_manpower = 3

add_permanent_province_modifier = {
	name = human_minority_oppressed_large
	duration = -1
}

1550.1.1 = {
	owner = Z50
	controller = Z50
	add_core = Z50
	culture = grombar_half_orc
	religion = corinite
}