
# No previous file for Banten
owner = R14
controller = R14
add_core = R14
culture = ghavaanaj
religion = high_philosophy

hre = no

base_tax = 2
base_production = 1
base_manpower = 2

trade_goods = livestock

capital = ""

is_city = yes

1550.1.1 = {
	owner = R62
	controller = R62
	add_core = R62
	religion = godlost
}
