
culture = fathide_ogre
religion = feast_of_the_gods
is_city = yes
owner = J40
controller = J40
add_core = J40


hre = no

base_tax = 1
base_production = 2
base_manpower = 2
native_size = 3
native_ferocity = 5
native_hostileness = 6

1550.1.1 = {
	owner = I90
	controller = I90
	add_core = I90
}

trade_goods = grain

capital = ""