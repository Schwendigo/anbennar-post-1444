
# No previous file for Urionmar
culture = green_orc
religion = great_dookan
capital = ""
tribal_owner = B30
hre = no

base_tax = 3
base_production = 2
base_manpower = 2

trade_goods = iron

native_size = 57
native_ferocity = 8
native_hostileness = 8

1550.1.1 = {
	owner = B02
	controller = B02
	add_core = B02
	is_city = yes
	religion = corinite
	culture = corintari
	tribal_owner = ---
}

add_permanent_province_modifier = {
	name = white_walls_of_castanor
	duration = -1
}