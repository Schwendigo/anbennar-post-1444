
# No previous file for Urumqi
owner = Z49
controller = Z49
add_core = Z49
culture = white_reachman
religion = regent_court

hre = no

base_tax = 4
base_production = 4
base_manpower = 4

trade_goods = iron

capital = ""

is_city = yes

add_permanent_province_modifier = {
	name = orcish_minority_oppressed_large
	duration = -1
}

1550.1.1 = {
	owner = Z50
	controller = Z50
	add_core = Z50
	culture = grombar_half_orc
	religion = corinite
}
