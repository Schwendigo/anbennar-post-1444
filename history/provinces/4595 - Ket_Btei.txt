owner = Y51
controller = Y51
add_core = Y51
culture = ranilau
religion = high_philosophy

hre = no

base_tax = 5
base_production = 4
base_manpower = 1

trade_goods = silk

capital = ""

is_city = yes

1550.1.1 = {
	owner = Y58
	controller = Y58
	add_core = Y58
}
fort_15th = yes



add_permanent_province_modifier = {
	name = harimari_minority_coexisting_small
	duration = -1
}