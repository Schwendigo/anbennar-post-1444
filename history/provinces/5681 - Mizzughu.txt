# No previous file for Nakhchivan

owner = U09
controller = U09
add_core = U09
culture = windhowler_gnoll
religion = xhazobkult
capital = ""

hre = no

base_tax = 1
base_production = 1
base_manpower = 2

trade_goods = livestock

is_city = yes

1550.1.1 = {
	owner = F66
	controller = F66
	add_core = F66
	religion = elikhetist
}
