



culture = trolltamer_goblin
religion = goblinic_shamanism
trade_goods = unknown
hre = no
base_tax = 1 
base_production = 1
base_manpower = 1
native_size = 57
native_ferocity = 10
native_hostileness = 10

add_permanent_province_modifier = {
	name = troll_minority_oppressed_large
	duration = -1
}

1550.1.1 = {
	owner = F47
	controller = F47
	add_core = F47
	religion = the_jadd
}