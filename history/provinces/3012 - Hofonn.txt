
owner = I29
controller = I29
add_core = I29
culture = wood_elf
religion = fey_court
hre = no
is_city = yes

1550.1.1 = {
	owner = U12
	controller = U12
	add_core = U12
}
trade_goods = cloth
base_tax = 4
base_manpower = 3
base_production = 4
native_size = 0
native_ferocity = 10
native_hostileness = 10

