



culture = undergrowth_goblin
religion = goblinic_shamanism
trade_goods = unknown
hre = no
base_tax = 1 
base_production = 1
base_manpower = 1
native_size = 89
native_ferocity = 10
native_hostileness = 10
add_permanent_province_modifier = {
	name = dwarovar_rail
	duration = -1
}

1550.1.1 = {
	owner = I18
	controller = I18
	add_core = I18
	culture = peridot_dwarf
	religion = ancestor_worship
}