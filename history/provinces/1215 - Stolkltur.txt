
# No previous file for Tajura
culture = teira
religion = dotimism
capital = ""

hre = no

base_tax = 1
base_production = 1
base_manpower = 1

trade_goods = unknown

native_size = 15
native_ferocity = 4
native_hostileness = 4
1550.1.1 = {
	owner = A13
	controller = A13
	add_core = A13
	is_city = yes
	culture = A13
	religion = A13
}