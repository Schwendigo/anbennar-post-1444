
owner = R66
controller = R66
add_core = R66
culture = undergrowth_goblin
religion = goblinic_shamanism
trade_goods = gems
hre = no
base_tax = 1 
base_production = 2
base_manpower = 1


1550.1.1 = {
	owner = R81
	controller = R81
	add_core = R81
	religion = godlost
	culture = march_goblin
}