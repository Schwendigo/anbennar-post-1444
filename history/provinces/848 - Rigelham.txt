
# No previous file for Tohancapan
culture = green_orc
religion = great_dookan
capital = ""
tribal_owner = B26
hre = no

base_tax = 1
base_production = 1
base_manpower = 1

trade_goods = fur

native_size = 25
native_ferocity = 8
native_hostileness = 8

1550.1.1 = {
	owner = Z37
	controller = Z37
	add_core = Z37
	is_city = yes
	religion = corinite
	culture = covenbladic
	tribal_owner = ---
}
latent_trade_goods = {
	coal
}