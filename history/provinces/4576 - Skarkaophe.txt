owner = Y57
controller = Y57
add_core = Y57
culture = gon
religion = righteous_path

hre = no

base_tax = 5
base_production = 5
base_manpower = 3

trade_goods = sugar

capital = ""

is_city = yes

1550.1.1 = {
	owner = R38
	controller = R38
	add_core = R38
}
