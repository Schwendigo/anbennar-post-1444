
# No previous file for Rottenstep

culture = green_orc
religion = great_dookan
capital = ""
trade_goods = salt
tribal_owner = B22

hre = no

base_tax = 1 
base_production = 1
base_manpower = 1

native_size = 30
native_ferocity = 10
native_hostileness = 6

1550.1.1 = {
	owner = B40
	controller = B40
	add_core = B40
	is_city = yes
	religion = corinite
	culture = alenori
	tribal_owner = ---
}